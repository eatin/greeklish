package greeklish

import (
	"strings"
	"unicode"

	"code.google.com/p/go/src/pkg/exp/norm"
)

const greek = "αβγδεζηθικλμνξοπρσςτυφχψω"

func isGreek(r rune) bool {
	return strings.ContainsRune(greek, unicode.ToLower(r))
}

// Not safe for use with diacritics
func elot743rune(s []rune, idx int) (x string, read int) {
	var p, r, r2, r3 rune
	if idx >= 1 {
		p = s[idx-1]
	}
	r = unicode.ToLower(s[idx])
	if idx+1 < len(s) {
		r2 = unicode.ToLower(s[idx+1])
		if idx+2 < len(s) {
			r3 = unicode.ToLower(s[idx+2])
		}
	}
	ruleAB := func(latin rune) (string, int) {
		const ruleAchars = "βγδζλμνραεηιοωυ"
		res := []rune{latin}
		if r2 == 'υ' {
			if strings.ContainsRune(ruleAchars, r3) {
				res = append(res, 'v')
			} else {
				res = append(res, 'f')
			}
		}
		return string(res), len(res)
	}
	switch r {
	case 'α':
		return ruleAB('a')
	case 'β':
		return "v", 1
	case 'γ':
		switch r2 {
		case 'γ':
			return "ng", 2
		case 'ξ':
			return "nx", 2
		case 'χ':
			return "nch", 2
		default:
			return "g", 1
		}
	case 'δ':
		return "d", 1
	case 'ε':
		return ruleAB('e')
	case 'ζ':
		return "z", 1
	case 'η':
		return ruleAB('i')
	case 'θ':
		return "th", 1
	case 'ι':
		return "i", 1
	case 'κ':
		return "k", 1
	case 'λ':
		return "l", 1
	case 'μ':
		if r2 == 'π' && (!isGreek(r3) || !isGreek(p)) {
			return "b", 2
		}
		return "m", 1
	case 'ν':
		return "n", 1
	case 'ξ':
		return "x", 1
	case 'ο':
		if r2 == 'υ' {
			return "ou", 2
		}
		return "o", 1
	case 'π':
		return "p", 1
	case 'ρ':
		return "r", 1
	case 'σ':
		return "s", 1
	case 'ς':
		return "s", 1
	case 'τ':
		return "t", 1
	case 'υ':
		return "y", 1
	case 'φ':
		return "f", 1
	case 'χ':
		return "ch", 1
	case 'ψ':
		return "ps", 1
	case 'ω':
		return "o", 1
	}
	// Return non-Greek characters verbatim
	return string(r), 1
}

// See <http://www.eki.ee/wgrs/rom1_el.htm>
// Strips diacritics
// TODO: Would be nice to expose a diacritic-safe version of this
func Elot743ND(s string) string {
	src := []rune{}
	for _, r := range norm.NFKD.String(s) {
		if !unicode.IsMark(r) {
			src = append(src, r)
		}
	}
	res := make([]string, 0, len(src))
	for i := 0; i < len(src); {
		x, y := elot743rune(src, i)
		i += y
		res = append(res, x)
	}
	return strings.Join(res, "")
}
